---
title: Trshnet.de Webseite vorrübergehend nicht erreichbar
date: 2018-12-24 20:00:00
resolved: true
resolvedWhen: 2018-12-27 10:57:00
# down, disrupted, notice
severity: disrupted
affected:
  - Trshnet.de
section: issue
---

*Lösung* -
Es wird davon ausgegangen, dass alle bisher auftretenden Störungen beseitigt werden konnten {{< track "2018-12-27 10:57:00" >}}

*Bearbeitung* - Es wird davon ausgegangen, dass es an einem Fehler des HAproxys liegt. Der Proxy scheint die anfragen nur sporadisch an den Backend Server durchzureichen {{< track "2018-12-27 10:55:00" >}}

*Ermittlung* - Es gibt wohl teilweile Probleme beim Aufrufen der Webseite. Es wird vermutet, dass es an den Zertifikaten in Verbindung
mit der Weiterleitung an den Webserver liegt. {{< track "2018-12-24 20:00:00" >}}
